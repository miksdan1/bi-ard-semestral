#include <Wire.h>
#include <BH1750.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>

BH1750 intensity_sensor; //BH1750 sensor
Adafruit_BME280 bme_sensor; //BME280 sensor
OneWire one_wire(0); //OneWire library on arduino pin 0
DallasTemperature ds_sensor(&one_wire); //DS18B20 sensor
#define BME280_ADRESS (0x76)

//network ssid and pass
const char* ssid = "Kikina123";
const char* password = "Risa01+-";

//webserver
AsyncWebServer server(80);

float mapf(float val, long min_i, long max_i, long min_o, float max_o)
{
 return (float)(val - min_i) * (max_o - min_o) / (float)(max_i - min_i) + min_o;
}

String get_value(const String& val){
  if(val == "TMP_BME"){
    Serial.println(bme_sensor.readTemperature());
    return String(bme_sensor.readTemperature());
  }
  else if (val == "TMP_DS"){
    ds_sensor.requestTemperatures();
    Serial.println(ds_sensor.getTempCByIndex(0));
    return String(ds_sensor.getTempCByIndex(0));
  }
  else if(val == "PRESS"){
    Serial.println(bme_sensor.readPressure()/ 100.0F);
    return String(bme_sensor.readPressure()/ 100.0F);
  }
  else if(val == "HUM"){
    Serial.println(bme_sensor.readHumidity());
    return String(bme_sensor.readHumidity());
  }
  else if(val == "LIGHT"){
    Serial.println(intensity_sensor.readLightLevel());
    return String(intensity_sensor.readLightLevel());
  }
  else if(val == "TMP_BME_W"){
    return String(mapf(bme_sensor.readTemperature(),-40,40,0, 0.5));
  }
  else if(val == "TMP_DS_W"){
    return String(mapf(ds_sensor.getTempCByIndex(0),-40,40,0, 0.5));
  }
  else if(val == "PRESS_W"){
    return String(mapf(bme_sensor.readPressure()/ 100.0F,300,1100,0, 0.5));
  }
  else if(val == "HUM_W"){
    return String(mapf(bme_sensor.readHumidity(),0,100,0, 0.5));
  }
  else if(val == "LIGHT_W"){
    return String(mapf(intensity_sensor.readLightLevel(),0,5000,0, 0.5));
  }
}

void setup() {
  //Start serial communication
  Serial.begin(115200);
  
  //Start BH 1750 sensor
  intensity_sensor.begin();
  //Start DS18B20 sensor
  ds_sensor.begin();
  //Start BME280 sensor
  if (!bme_sensor.begin(BME280_ADRESS)){
    Serial.println("BME280 Not found!");
    while (1);
  }
  if(!SPIFFS.begin()){
    Serial.println("SPIFFS error");
    return;
  }

  //Connect ESP to Wifi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting..");
  }
  Serial.println("-----------------");
  Serial.println("connected to Wifi");
  Serial.println("IP:");
  Serial.println(WiFi.localIP());
  Serial.println("-----------------");
  
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, get_value);
  });
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });
  server.begin();
}

void loop() {
  
}
